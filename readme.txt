Usage:
./build.sh test test.olice.com "Test" ~/dara/dev/m1

Arguments:
1: The folder that will be created
2: The unique app ID, eg com.olice.app
3: The app name, eg "Test App"
4: The source folder for the HTML app, eg ~/my-app/www/