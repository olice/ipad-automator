#!/bin/sh
 
DIR="$1"
APPID="$2"
TITLE="$3"
SRC="$4"

# Starting
echo "Title: $TITLE \nDirectory: $DIR \nApp ID: $APPID \nSource: $SRC";

# Create cordova app
echo "Creating Cordova project"
cordova create "$DIR" "$APPID" "$TITLE"

# Update config
echo "Updating config file with preferences"
cp "./config-template.xml" "./$DIR/config.xml"
sed -i '' "s/{APPID}/$APPID/g; s/{TITLE}/$TITLE/g;" "./$DIR/config.xml"

# Rename the www dir Cordova created
echo "Renaming www directory auto-created by Cordova"
mv "$DIR/www" "$DIR/www-orig"

# Create symbolic link from source project
echo "Creating symbolic link from source directory to www"
ln -s "$SRC" "./$DIR/www"

# Copy splash and icon PNGs from source dir
echo "Copying icon.png"
cp "$SRC/icon.png" "./$DIR/icon.png"
echo "Copying splash.png"
cp "$SRC/splash.png" "./$DIR/splash.png"

# Move into new directory
echo "Changing directory to $DIR"
cd "./$DIR"

# Add iOS
echo "Adding iOS platform"
cordova platform add ios 

# Install custom config plugin
cordova plugin add cordova-custom-config

#Add Cordova splash and icon plugins
echo "Adding icon"
cordova-icon
echo "Adding splash"
cordova-splash

echo "Building project"
cordova build

#Open new project in XCode
echo "Opening XCode project"
open "platforms/ios/$TITLE.xcodeproj"












#FIRST_ARGUMENT="$1"
#echo "Hello, world $FIRST_ARGUMENT!"